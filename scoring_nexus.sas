cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;
/*Pegando variável de ambiente com o número do build no jenkins e referenciando o arquivo astore com sua versão correta*/
%let y =/tmp/astore_master-;
%let i = %sysget(BUILD_ID);
%put i=&i;


%let result=&y&i;

%put result=&result;



/*Proc upload com astore, file -> caslib*/
proc astore;
    upload rstore=public.astore_gb_NEXUS
             store="&result."; 
quit;

/*Scorando com Astore*/
proc astore;
score data=public.HMEQ_TRATADA out=public.HMEQ_SCORED_ASTORE
rstore=public.astore_gb_NEXUS;
quit;

proc casutil
incaslib="PUBLIC" outcaslib="PUBLIC";                         
promote casdata="HMEQ_SCORED_ASTORE";
run;

