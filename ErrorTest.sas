cas mysess;
caslib _ALL_ assign;

%*let debug=CANCEL;
%let debug=;

proc cas;
/* Checar se a tabela existe */
 table.tableexists result=r / /* 2 */
 caslib='PUBLIC'
 name='HMEQ';
 run &debug.;
	if (r.exists) then do; /* 3 */ 
			table.dropTable result=r
			caslib="PUBLIC"
			name="HMEQ";
	end;
quit;
/* Teste */
/* Promoting to CAS  */
proc casutil;
                load file="/opt/sas/viya/config/data/cas/default/public/HMEQ.sashdat" incaslib="PUBLIC" 
                outcaslib="PUBLIC" casout="HMEQ" promote;
if _error_ = 1 then stop;
run &debug.;


/*Testing sintaxe*/
data _null_;
 do i = 1 to 1000000000;
 n1=rnd('uniform');
 output;
 end;
run &debug.;